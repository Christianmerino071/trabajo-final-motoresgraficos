using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public float rapidezDesplazamiento = 8.0f;
    public Camera RayCasting;
    public int RapidezCorrer;
    public GameObject Pistol;
    public GameObject pistolPiso;
    public CapsuleCollider col;
    public CapsuleCollider colBot;
    public  GameObject Linterna;
    public GameObject Llave;
    public bool encendida;
    public TMPro.TMP_Text Ganaste;
    public TMPro.TMP_Text objetivo;
    public GameObject CameraPrimeraPersona;
    public GameObject EspadaLaser;
   

    public GameObject PressE;

    public int Hp = 100;
    public TMPro.TMP_Text Vida;
    private int cont = 100;





    public float x, y;

    private Animator anim;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        
        objetivo.text = "busca la llaves del auto y escapa";
        Vida.text = "";
        Hp = 100;
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        setearTextos();
        
        float movimientoHorizontal = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        float movimientoVertical = Input.GetAxis("Vertical") * rapidezDesplazamiento;

        movimientoHorizontal *= Time.deltaTime;
        movimientoVertical *= Time.deltaTime;

        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);





        transform.Translate(movimientoHorizontal, 0, movimientoVertical);

        

        if (Input.GetKey(KeyCode.LeftShift))

        {
            rapidezDesplazamiento = RapidezCorrer;
            if (y > 0)
            {
                
                anim.SetBool("Correr", true);
            }
            else
            {
                anim.SetBool("Correr", false);
            }


        }
        else
        {
            anim.SetBool("Correr", false);

            rapidezDesplazamiento = rapidezDesplazamiento = 8.0f;
        }



        if (Input.GetMouseButton(1))
        {
            anim.SetBool("Apuntar", true);
        }
        else
        {
            anim.SetBool("Apuntar", false);
        }


        if (Input.GetKey(KeyCode.LeftControl))
        {
            anim.SetBool("Agachado", true);


        }
        else
        {
            anim.SetBool("Agachado", false);
        }

        if (Input.GetKey(KeyCode.F))
        {
            Linterna.SetActive(!Linterna.activeSelf);

        }
        


        if (Input.GetKey(KeyCode.R))
        {
            EspadaLaser.SetActive(true);
            Pistol.SetActive(false);
            
        }
        if (Input.GetKey(KeyCode.T))
        {
            Pistol.SetActive(true);
            EspadaLaser.SetActive(false);
        }

        if (Input.GetMouseButton(0) && !Input.GetMouseButton(1))
        {
            anim.SetBool("GolpearEspada", true);
        }
        else
        {
            anim.SetBool("GolpearEspada", false);
        }













    }
    private void OnTriggerEnter(Collider col)
    {
        


        if (col.gameObject.CompareTag("Da�o") == true)
        {
            Hp = Hp - 25;
            cont = cont - 25;
            
            if (Hp <= 0)
            {
                Vector3 posicion = new Vector3(0, 1.21f, -25.03f);
                transform.position = transform.position = posicion;

                Hp = 100;
                cont = 100;
            }


        }

        
        



    }

    private void OnTriggerStay(Collider col)
    {

        if (col.gameObject.CompareTag("PosicionAgarrar") == true && Input.GetKey(KeyCode.E))
        {

            
            pistolPiso.SetActive(false);
            Pistol.SetActive(true);
            EspadaLaser.SetActive(false);

        }

        if (col.gameObject.CompareTag("AgarrarLlave") == true && Input.GetKey(KeyCode.E))
        {
            Llave.SetActive(false);
            objetivo.text = "Corre hacia el auto!!";
        
        }
        if (col.gameObject.CompareTag("EscapaAuto") == true && Input.GetKey(KeyCode.E))
        {
            Ganaste.text = "SOBREVIVISTE!!!";
        }
        if (col.gameObject.CompareTag("PresionaE") == true)
        {
            PressE.SetActive(true);
            Destroy(PressE, 5);

        }

        
        


    }

    private void setearTextos()
    {
      Vida.text = "Vida:" + cont.ToString();      
    }






}
