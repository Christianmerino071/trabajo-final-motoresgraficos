using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameManager : MonoBehaviour
{
    public GameObject jugador;
    public GameObject bot;
  
    float tiempoRestante;
    public TMPro.TMP_Text TiempoRestante;
    private int cont = 200;
    public GameObject Llave;

    private void Start()
    {
        TiempoRestante.text = "";
        ComenzarJuego();
      
    }
    void Update()
    {
        SetearTextos();

        if (tiempoRestante == 0)
        {
            ComenzarJuego();
        }

       



    }

    void ComenzarJuego()
    {

        jugador.transform.position = new Vector3(-2.5f, 1.42f, -66.95f);

        StartCoroutine(ComenzarCronometro(200));
    }
    public IEnumerator ComenzarCronometro(float valorCronometro = 200)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            cont = cont - 1;

            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }

    private void SetearTextos()
    {
        TiempoRestante.text = "TiempoRestante: " + cont.ToString();

    }

   

}