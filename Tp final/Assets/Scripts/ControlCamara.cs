using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    Vector2 mouseMirar;
    Vector2 suavidadV;

    public Transform PrimeraP, TerceraP;
    private bool vista;
    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;

    public Camera RayCasting;


    GameObject jugador;

    void Start()
    {
        jugador = this.transform.parent.gameObject;
    }

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 2f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 2f / suavizado);

        mouseMirar += suavidadV;
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90f, 90f);
        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, jugador.transform.up);

        if (Input.GetMouseButtonDown(0) && Input.GetMouseButton(1))
        {
            Ray ray = RayCasting.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 60)
            {
                Debug.Log("Daņaste a: " + hit.collider.name);
                if (hit.collider.name.Substring(0, 3) == "Bot")
                {
                    GameObject objetoTocado = GameObject.Find(hit.transform.name);
                    ControlBot scriptObjetoTocado = (ControlBot)objetoTocado.GetComponent(typeof(ControlBot));

                    if (scriptObjetoTocado != null)
                    {
                        scriptObjetoTocado.recibirDaņo();
                    }

                }
            }

          

        }

        AtacaCuerpoACuerpo();


        CambioDeCamara();

       

    }

    private void CambioDeCamara()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            vista = true;

        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            vista = false;
        }

        if (vista == true)
        {
            transform.position = Vector3.Lerp(transform.position, PrimeraP.position, 5 * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, TerceraP.position, 5 * Time.deltaTime);
        }

    }

    private void AtacaCuerpoACuerpo()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = RayCasting.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 10)
            {
                Debug.Log("ataco CuerpoAcuerpo a: " + hit.collider.name);
                if (hit.collider.name.Substring(0, 3) == "Bot")
                {
                    GameObject objetoTocado = GameObject.Find(hit.transform.name);
                    ControlBot scriptObjetoTocado = (ControlBot)objetoTocado.GetComponent(typeof(ControlBot));

                    if (scriptObjetoTocado != null)
                    {
                        scriptObjetoTocado.recibirDaņo();
                    }

                }
            }

        }
    }


}

