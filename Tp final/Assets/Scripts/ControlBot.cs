using System.Runtime.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEditor.PlayerSettings;

public class ControlBot : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public int rapidez;
    private Animator anim;
    public float x, y;
    public GameObject Bot;
    public BoxCollider coll;
    public CapsuleCollider col;
   





    void Start()
    {
        hp = 100;
        //Otra forma de obtener un GameObject sin arrastrarlo desde el editor
        jugador = GameObject.Find("Jugador");
        anim = GetComponent<Animator>();
        


    }
    private void Update()
    {
        

        anim.SetFloat("VelX", rapidez);
        anim.SetFloat("VelY", rapidez);
    }
    public void recibirDaņo()
    {
        hp = hp - 25;
        if (hp <= 0)
        {
            
            anim.SetBool("Morir", true);
            Destroy(Bot ,3);
        }
    }
    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnTriggerStay(Collider coll)
    {
        

        if (coll.gameObject.CompareTag("Player") == true )
        {
       
                transform.LookAt(jugador.transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

        }

        

    }

  

}